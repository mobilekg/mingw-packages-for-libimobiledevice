#!/bin/bash

# AppVeyor and Drone Continuous Integration for MSYS2
# Author: Renato Silva <br.renatosilva@gmail.com>
# Author: Qian Hong <fracting@gmail.com>

# Configure
cd "$(dirname "$0")"
source 'ci-library.sh'
BUILD_URL=dummy # Hack deploy_enabled() & create_build_references()
deploy_enabled && mkdir artifacts
# git_config user.email 'ci@msys2.org'
# git_config user.name  'MSYS2 Continuous Integration'
# git remote add upstream 'https://github.com/MSYS2/MINGW-packages'
# git remote add upstream 'https://gitlab.com/mobilekg/mingw-packages-for-libimobiledevice.git'
# git fetch --quiet upstream

# reduce time required to install packages by disabling pacman's disk space checking
sed -i 's/^CheckSpace/#CheckSpace/g' /etc/pacman.conf

# Detect
# list_commits  || failure 'Could not detect added commits'
# list_packages || failure 'Could not detect changed files'
# message 'Processing changes' "${commits[@]}"

# Increase pacman download speed
# https://linuxsuperuser.com/increase-pacman-download-speed-in-arch-linux/
# cp /etc/pacman.d/mirrorlist.mingw32 /etc/pacman.d/mirrorlist.mingw32.backup
# sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.mingw32.backup
# rankmirrors -n 6 /etc/pacman.d/mirrorlist.mingw32.backup > /etc/pacman.d/mirrorlist.mingw32

# cp /etc/pacman.d/mirrorlist.mingw64 /etc/pacman.d/mirrorlist.mingw64.backup
# sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.mingw64.backup
# rankmirrors -n 6 /etc/pacman.d/mirrorlist.mingw64.backup > /etc/pacman.d/mirrorlist.mingw64

# cp /etc/pacman.d/mirrorlist.msys /etc/pacman.d/mirrorlist.msys.backup
# sed -i 's/^#Server/Server/' /etc/pacman.d/mirrorlist.msys.backup
# rankmirrors -n 6 /etc/pacman.d/mirrorlist.msys.backup > /etc/pacman.d/mirrorlist.msys

# Hack packages
packages=(\
"mingw-w64-libplist" \
)
# "mingw-w64-libusbmuxd" \
# "mingw-w64-libimobiledevice" \
# "mingw-w64-usbmuxd" \
# "mingw-w64-libusb" \
# "mingw-w64-libusb-compat-git" \
# "mingw-w64-openssl" 
test -z "${packages}" && success 'No changes in package recipes'
define_build_order || failure 'Could not determine build order'

DISABLE_QUALITY_CHECK=true # Hack to skip check_recipe_quality and tweak update_system

# Build
message 'Building packages' "${packages[@]}"
execute 'Updating system' update_system
execute 'Approving recipe quality' check_recipe_quality
for package in "${packages[@]}"; do
    execute 'Building binary' makepkg-mingw --noconfirm --noprogressbar --skippgpcheck --nocheck --syncdeps --rmdeps --cleanbuild
    execute 'Building source' makepkg --noconfirm --noprogressbar --skippgpcheck --allsource --config '/etc/makepkg_mingw64.conf'
    execute 'Installing' yes:pacman --noprogressbar --upgrade *"${PKGEXT}"
    deploy_enabled && mv "${package}"/*"${PKGEXT}" artifacts
    deploy_enabled && mv "${package}"/*"${SRCEXT}" artifacts
    unset package
done

# Deploy
deploy_enabled && cd artifacts || success 'All packages built successfully'
# execute 'Generating pacman repository' create_pacman_repository "${PACMAN_REPOSITORY_NAME:-ci-build}"
# execute 'Generating build references'  create_build_references  "${PACMAN_REPOSITORY_NAME:-ci-build}"
execute 'SHA-256 checksums' sha256sum *
success 'All artifacts built successfully'
